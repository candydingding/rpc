package single.rpc.transport;

import jdk.internal.util.xml.impl.Input;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
public interface RequestHandler {
    void onRequest(InputStream recive, OutputStream toRespon);

}
