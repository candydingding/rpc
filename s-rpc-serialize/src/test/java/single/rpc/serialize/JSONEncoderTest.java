package single.rpc.serialize;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
public class JSONEncoderTest {

    @Test
    public void encode() {
        Encoder encoder = new JSONEncoder();
        TestBean bean = new TestBean();
        bean.setName("JJ划水");
        bean.setAge(18);
        byte[] encode = encoder.encode(bean);
        assertNotNull(encode);
    }
}