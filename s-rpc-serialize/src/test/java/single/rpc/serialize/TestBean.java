package single.rpc.serialize;

import lombok.Data;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
@Data
public class TestBean {
    private String name;
    private int age;
}
