package single.rpc.serialize;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
public class JSONDecoderTest {

    @Test
    public void decode() {
        Encoder encoder = new JSONEncoder();
        Decoder decoder = new JSONDecoder();
        TestBean bean = new TestBean();
        bean.setName("JJ划水");
        bean.setAge(18);
        byte[] encode = encoder.encode(bean);


        TestBean decode = decoder.decode(encode, TestBean.class);
        assertEquals(decode.getName(),bean.getName());
        assertEquals(decode.getAge(), bean.getAge());
    }
}