package single.rpc.server;

import single.rpc.Request;
import single.rpc.common.ReflectionUtils;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
public class ServiceInvoker {
    public Object invoke(ServiceInstance instance, Request request) {
        return ReflectionUtils.invoke(
                instance.getTarget(),
                instance.getMethod(),
                request.getParameters()
        );
    }
}
