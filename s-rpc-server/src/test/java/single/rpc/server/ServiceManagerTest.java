package single.rpc.server;

import org.junit.Before;
import org.junit.Test;
import single.rpc.Request;
import single.rpc.ServiceDescriptor;
import single.rpc.common.ReflectionUtils;

import java.lang.reflect.Method;

import static org.junit.Assert.*;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe 
 * @Date 2022/4/4
 */
public class ServiceManagerTest {

    ServiceManager sm;
    @Before
    public void init(){
        sm = new ServiceManager();
        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }
    @Test
    public void register() {
        TestInterface bean = new TestClass();
        sm.register(TestInterface.class, bean);
    }

    @Test
    public void lookup() {
        Method[] methods = ReflectionUtils.getPublicMethods(TestInterface.class);
        ServiceDescriptor sdp = ServiceDescriptor.from(TestInterface.class, methods[0]);
        Request request = new Request();
        request.setService(sdp);
        ServiceInstance lookup = sm.lookup(request);
        assertNotNull(lookup);
    }
}