package single.rpc.common;

/**
 * @Author CandyDingDing
 * @Version 1.0
 * @Motto 且视他人之疑目如盏盏鬼火,大胆地去走吾之夜路
 * @Describe
 * @Date 2022/4/4
 */
public class TestClass {
    public String a(){
        return "a";
    }
    protected String b(){
        return "b";
    }
    private String c(){
        return "c";
    }
}
